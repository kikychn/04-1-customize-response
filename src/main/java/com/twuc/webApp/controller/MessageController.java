package com.twuc.webApp.controller;

import com.twuc.webApp.sources.Message;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api")
public class MessageController {

    @PostMapping("/no-return-value")
    public void noReturnValue() {
    }

    @PostMapping("/no-return-value-with-annotation")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void noReturnValueWithAnnotation() {
    }

    @PostMapping("/messages/{message}")
    public String getMessageString(@PathVariable String message) {
        return message;
    }

    @PostMapping("/message-objects/{message}")
    public Message getMessageObject(@PathVariable String message) {
        return new Message(message);
    }

    @PostMapping("/message-objects-with-annotation/{message}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Message getMessageObjectWithAnnotation(@PathVariable String message) {
        return new Message(message);
    }

    @PostMapping("/message-entities/{message}")
    public ResponseEntity<Message> getMessageResponseEntity(@PathVariable String message) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .header("X-Auth", "me")
                .contentType(MediaType.APPLICATION_JSON)
                .body(new Message(message));
    }

}
