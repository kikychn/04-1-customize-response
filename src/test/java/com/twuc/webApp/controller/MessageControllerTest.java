package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class MessageControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void no_return_value() throws Exception {
        mockMvc.perform(post("/api/no-return-value"))
                .andExpect(status().is(200));
    }

    @Test
    void no_return_value_with_annotation() throws Exception {
        mockMvc.perform(post("/api/no-return-value-with-annotation"))
                .andExpect(status().is(204));
    }

    @Test
    void should_return_message_string() throws Exception {
        mockMvc.perform(post("/api/messages/hello"))
                .andExpect(status().is(200))
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("hello"));
    }

    @Test
    void should_return_message_object() throws Exception {
        mockMvc.perform(post("/api/message-objects/hello"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.value").value("hello"));
    }

    @Test
    void should_return_message_object_with_annotation() throws Exception {
        mockMvc.perform(post("/api/message-objects-with-annotation/hello"))
                .andExpect(status().is(202))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.value").value("hello"));
    }

    @Test
    void should_return_header_contain_xauth() throws Exception {
        mockMvc.perform(post("/api/message-entities/hello"))
                .andExpect(header().string("X-Auth","me"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.value").value("hello"));
    }
}
